package com.codetest.attendee.controller;

import com.codetest.attendee.domain.AttendeeDTO;
import com.codetest.attendee.exception.AttendeeException;
import com.codetest.attendee.rest.RestFulResponse;
import com.codetest.attendee.service.AttendeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(path = "/attendee")
public class AttendeeController {

	@Autowired
	private AttendeeService attendeeService;

	@RequestMapping(params = "name", method = RequestMethod.GET, produces = "text/plain")
	public RestFulResponse getAttendeeByName(@RequestParam("name") String name) {

		final Optional<AttendeeDTO> attendeeByName = attendeeService.getAttendeeByName(name);
		String responseBody;
		if (attendeeByName.isPresent()) {
			responseBody = attendeeByName.get().toString();
		} else {
			responseBody = "The requested attendee doesn't exists in our DB";
		}

		return RestFulResponse.buildOKResponseWithBody(responseBody);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "text/plain")
	public RestFulResponse getAllAttendees() {
		final Collection<AttendeeDTO> allAttendees = attendeeService.getAllAttendees();
		return RestFulResponse.buildOKResponseWithBody(allAttendees.toString());
	}

	@RequestMapping(method = RequestMethod.POST, headers = { "Content-type=application/json" }, produces = "text/plain")
	public RestFulResponse save(@RequestBody AttendeeDTO attendee) {

		attendeeService.saveAttendee(attendee);
		return RestFulResponse.buildOKResponseWithBody("attendee inserted correctly");
	}
/*
	@RequestMapping(method = RequestMethod.POST, produces = "text/plain")
	public RestFulResponse update(@RequestBody AttendeeDTO attendee) {

		Optional<AttendeeDTO> attendeeFound = attendeeService.getAttendeeByNameAndLastName(attendee.getName(),attendee.getLastName());
		String responseBody;
		
		if (!attendeeFound.isPresent()) {
			responseBody = "The requested attendee doesn't exists in our DB";
		} else {
			attendeeService.updateAttendee(attendee);
			responseBody = "attendee updated correctly";
		}
		return RestFulResponse.buildOKResponseWithBody(responseBody);
	}
*/
	@RequestMapping(method = RequestMethod.POST, produces = "text/plain")
	public RestFulResponse cleanAllAttendees() {
		attendeeService.cleanAttendees();
		return RestFulResponse.buildOKResponseWithBody("attendees clean correctly");
	}

	@RequestMapping(params = "name", method = RequestMethod.POST, produces = "text/plain")
	public RestFulResponse removeAttendeeByNameLastNameCompany(@RequestParam("name") String name,
			@RequestParam("lastName") String lastName, @RequestParam("company") String company) {

		Optional<AttendeeDTO> attendee = attendeeService.getAttendeeByNameAndLastName(name, lastName);
		String responseBody;
		if (!attendee.isPresent()) {
			responseBody = "The requested attendee doesn't exists in our DB";
		} else {
			attendeeService.removeAttendee(attendee.get());
			responseBody = "Attendee removed";
		}

		return RestFulResponse.buildOKResponseWithBody(responseBody);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Unexpected error on the server")
	@ExceptionHandler(Exception.class)
	public void unExpectedExceptionInTheServer() {
		// Nothing to do
	}

	@ResponseStatus(value = HttpStatus.OK)
	@ExceptionHandler(AttendeeException.class)
	public RestFulResponse duplicateAttendeeInsertion() {
		return RestFulResponse.buildFailResponseWithBody(
				"the request can't be completed because an attendee with that name exists in DB");
	}

}
