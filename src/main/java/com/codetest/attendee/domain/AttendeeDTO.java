package com.codetest.attendee.domain;

import java.util.Objects;

public class AttendeeDTO {

	private String name;
	private String lastName;
	private String company;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, lastName, company);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final AttendeeDTO other = (AttendeeDTO) obj;
		return Objects.equals(this.name, other.name) && Objects.equals(this.lastName, other.lastName)
				&& Objects.equals(this.company, other.company);
	}

	@Override
	public String toString() {
		return "Attendee{" + "name='" + name + '\'' + "lastName='" + lastName + '\''+ "company='" + company + '}';
	}
}
