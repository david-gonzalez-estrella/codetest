package com.codetest.attendee.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.codetest.attendee.domain.AttendeeDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Component
public class AttendeeDAO {
	private static Logger LOG = LoggerFactory.getLogger(AttendeeDAO.class);
	private static Set<AttendeeDTO> attendees = Collections.synchronizedSet(new HashSet<>());

	public void saveAttendee(AttendeeDTO attendee) {
		attendees.add(attendee);
		LOG.info("attendee [{}] added to the data store", attendee);
	}

	public Optional<AttendeeDTO> getAttendeeByName(String attendeeName) {
		return attendees.stream().filter(attendeeMatchNameDto(attendeeName)).findFirst();
	}

	public Optional<AttendeeDTO> getAttendeeByNameAndLastName(String attendeeName, String attendeeLastName) {

		AttendeeDTO toReturn = null;

		for (AttendeeDTO attendee : attendees) {
			if (attendee.getName().equalsIgnoreCase(attendeeName)
					&& attendee.getLastName().equalsIgnoreCase(attendeeLastName)) {
				toReturn = attendee;
				break;
			}
		}

		return Optional.ofNullable(toReturn);
	}
	
	public void updateAttendee(AttendeeDTO attendee){
		Optional<AttendeeDTO> attendeeFound = getAttendeeByNameAndLastName(attendee.getName(), attendee.getLastName());
		
		if(attendeeFound.isPresent()){
			attendeeFound.get().setCompany(attendee.getCompany());
		}
	}

	public void removeAttendee(AttendeeDTO attendee) {
		attendees.remove(attendee);
	}

	private static Predicate<AttendeeDTO> attendeeMatchNameDto(final String attendeeName) {
		return (AttendeeDTO attendee) -> Objects.equals(attendee.getName(), attendeeName);
	}

	public Collection<AttendeeDTO> getAll() {
		return Collections.unmodifiableCollection(attendees);
	}

	public void cleanAll() {
		attendees.clear();
	}
}
