package com.codetest.attendee.rest;

public class RestFulResponse {
	public static final String OK_RESPONSE = "OK";
	public static final String FAIL = "Fail";
	private final String code;
	private final String body;

	public RestFulResponse(String code, String body) {
		this.code = code;
		this.body = body;
	}

	public static RestFulResponse buildOKResponseWithBody(String body) {
		return new RestFulResponse(OK_RESPONSE, body);
	}

	public static RestFulResponse buildFailResponseWithBody(String body) {
		return new RestFulResponse(FAIL, body);
	}

	public String getCode() {
		return code;
	}

	public String getBody() {
		return body;
	}
}
