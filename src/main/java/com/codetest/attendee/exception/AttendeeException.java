package com.codetest.attendee.exception;


public class AttendeeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AttendeeException() {
    }

    public AttendeeException(String message) {
        super(message);
    }

    public AttendeeException(String message, Throwable cause) {
        super(message, cause);
    }

    public AttendeeException(Throwable cause) {
        super(cause);
    }

    public AttendeeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
