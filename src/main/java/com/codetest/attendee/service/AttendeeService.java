package com.codetest.attendee.service;

import com.codetest.attendee.dao.AttendeeDAO;
import com.codetest.attendee.domain.AttendeeDTO;
import com.codetest.attendee.exception.AttendeeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class AttendeeService {

	@Autowired
	private AttendeeDAO attendeeDAO;

	public void saveAttendee(AttendeeDTO attendee) {
		final Optional<AttendeeDTO> attendeeByName = attendeeDAO.getAttendeeByName(attendee.getName());
		if (attendeeByName.isPresent()) {
			throw new AttendeeException(
					String.format("A attendee with this name %s already exits", attendee.getName()));
		} else {
			attendeeDAO.saveAttendee(attendee);
		}
	}

	public void updateAttendee(AttendeeDTO attendee) {
		attendeeDAO.updateAttendee(attendee);
	}

	public Optional<AttendeeDTO> getAttendeeByName(String name) {
		return attendeeDAO.getAttendeeByName(name);
	}

	public Optional<AttendeeDTO> getAttendeeByNameAndLastName(String attendeeName, String attendeeLastName) {
		return attendeeDAO.getAttendeeByNameAndLastName(attendeeName, attendeeLastName);
	}

	public Collection<AttendeeDTO> getAllAttendees() {
		return attendeeDAO.getAll();
	}

	public void removeAttendee(AttendeeDTO attende) {
		attendeeDAO.removeAttendee(attende);
	}

	public void cleanAttendees() {
		attendeeDAO.cleanAll();
	}

}
