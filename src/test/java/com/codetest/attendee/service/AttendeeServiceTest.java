package com.codetest.attendee.service;

import com.codetest.attendee.dao.AttendeeDAO;
import com.codetest.attendee.domain.AttendeeDTO;
import com.codetest.attendee.exception.AttendeeException;
import com.codetest.attendee.service.AttendeeService;
import com.codetest.attendee.util.DomainBuilder;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AttendeeServiceTest {
    @Mock
    private AttendeeDAO attendeeDAO;

    @InjectMocks
    private AttendeeService victim;

    @Rule
    public ExpectedException expected =ExpectedException.none();

    private AttendeeDTO attendeeDTO;

    @Test
    public void test_saveAttendee_givenRepeatedAttendee_thenDuplicateAttendeeExceptionIsThrown(){
        givenAttendee();
        givenAttendeeWhenInDAOResponse();
        givenDuplicateAttendeeExceptionIsThrown();
        whenSaveAttendee();
        thenMockGetAttendeeByNameGetsCalled();
    }

    private void thenMockGetAttendeeByNameGetsCalled() {
        Mockito.verify(attendeeDAO,times(1)).getAttendeeByName(attendeeDTO.getName());
    }

    private void givenDuplicateAttendeeExceptionIsThrown() {
        expected.expect(AttendeeException.class);
        expected.expectMessage(containsString(attendeeDTO.getName()));
    }

    private void whenSaveAttendee() {
        victim.saveAttendee(attendeeDTO);
    }

    private void givenAttendeeWhenInDAOResponse() {
        Mockito.when(attendeeDAO.getAttendeeByName(attendeeDTO.getName())).thenReturn(Optional.of(attendeeDTO));
    }

    private void givenAttendee() {
        attendeeDTO = DomainBuilder.buildAttendee("david","gonzalez","telefonica");
    }

    @Test
    public void test_saveAttendee_nonDuplicateAttendee_thenSavedMethodGetsCalled(){
        givenAttendee();
        givenNonPresentAttendeeInDAOResponse();
        whenSaveAttendee();
        thenMockGetAttendeeByNameGetsCalled();
        thenMockSaveAttendeeGetsCalled();

    }

    private void thenMockSaveAttendeeGetsCalled() {
        Mockito.verify(attendeeDAO,times(1)).saveAttendee(attendeeDTO);
    }

    private void givenNonPresentAttendeeInDAOResponse() {
        Mockito.when(attendeeDAO.getAttendeeByName(attendeeDTO.getName())).thenReturn(Optional.empty());
    }


}