package com.codetest.attendee.controller;

import com.codetest.attendee.controller.AttendeeController;
import com.codetest.attendee.domain.AttendeeDTO;
import com.codetest.attendee.rest.RestFulResponse;
import com.codetest.attendee.service.AttendeeService;
import com.codetest.attendee.util.DomainBuilder;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AttendeeControllerTest {

	@InjectMocks
	private AttendeeController victim;

	@Mock
	private AttendeeService attendeeService;
	@Mock(answer = Answers.CALLS_REAL_METHODS)
	private AttendeeDTO atendeeDto;
	private String attendeeName;
	private RestFulResponse response;

	@Test
	public void testSaveAttendee_givenValidAtendee_thenAtendeeServiceGetsInvoked() {
		givenValidAttendee();
		whenSaveMethodGetsCalled();
		thenAttendeeServiceGetsInvoked();
	}

	private void thenAttendeeServiceGetsInvoked() {
		Mockito.verify(attendeeService, times(1)).saveAttendee(atendeeDto);
	}

	private void whenSaveMethodGetsCalled() {
		victim.save(atendeeDto);
	}

	private void givenValidAttendee() {
		atendeeDto = DomainBuilder.buildAttendee("david", "gonzalez", "telefonica");
	}

	@Test
	public void testGetAttendeeByName_givenUnKnownName_thenReturnsOKWithValidErrorMessage() throws Exception {
		givenAttendeeName("Pepe");
		givenEmptyResponseFromService(Optional.empty());
		whenGetAttendeeByName();
		thenOKResponseWithBody(is("The requested attendee doesn't exists in our DB"));
		thenVerifyMockServiceGetsInvoked();
	}

	private void thenVerifyMockServiceGetsInvoked() {
		Mockito.verify(attendeeService, times(1)).getAttendeeByName(attendeeName);
	}

	private void thenOKResponseWithBody(Matcher<String> expectedBody) {
		assertThat(response.getBody(), expectedBody);
		assertThat(response.getCode(), is("OK"));
	}

	private void whenGetAttendeeByName() {
		response = victim.getAttendeeByName(attendeeName);
	}

	private void givenEmptyResponseFromService(Optional<AttendeeDTO> serviceResponse) {
		Mockito.when(attendeeService.getAttendeeByName(attendeeName)).thenReturn(serviceResponse);
	}

	private void givenAttendeeName(String name) {
		attendeeName = name;
	}

	@Test
	public void testGetAttendeeByName_givenKnownName_thenReturnsOKWithAttendeeInBody() throws Exception {
		givenAttendeeName("david");
		givenValidAttendee();
		givenEmptyResponseFromService(Optional.of(atendeeDto));
		whenGetAttendeeByName();
		thenOKResponseWithBody(containsString("david"));
		thenVerifyMockServiceGetsInvoked();
	}
}