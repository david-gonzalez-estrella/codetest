package com.codetest.attendee.util;

import com.codetest.attendee.domain.AttendeeDTO;

public class DomainBuilder {

	public static AttendeeDTO buildAttendee(String name, String lastName, String company) {

		AttendeeDTO attendee = new AttendeeDTO();
		attendee.setName(name);
		attendee.setLastName(lastName);
		attendee.setCompany(company);
		return attendee;
	}

}
