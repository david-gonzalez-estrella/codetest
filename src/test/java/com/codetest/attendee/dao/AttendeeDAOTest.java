package com.codetest.attendee.dao;

import com.codetest.attendee.dao.AttendeeDAO;
import com.codetest.attendee.domain.AttendeeDTO;
import com.codetest.attendee.util.DomainBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertSame;

@RunWith(MockitoJUnitRunner.class)
public class AttendeeDAOTest {
	@InjectMocks
	private AttendeeDAO victim;
	private Optional<AttendeeDTO> attendeeResult;

	@Test
	public void testGetAttendeeByName_givenNotMatchingName_ThenReturnsEmptyOptional() {
		givenAttendeeWithName("david");
		whenGetAttendeeByName("jose");
		thenResultIsEmpty();
	}

	private void thenResultIsEmpty() {
		assertThat(attendeeResult.isPresent(), is(false));
	}

	private void whenGetAttendeeByName(String name) {
		attendeeResult = victim.getAttendeeByName(name);
	}

	private void givenAttendeeWithName(String name) {
		victim.saveAttendee(DomainBuilder.buildAttendee(name, "", ""));
	}

	@Test
	public void testGetAttendeeByName_givenMatchingName_ThenReturnsCorrectAttendee() {
		givenAttendeeWithName("david");
		whenGetAttendeeByName("david");
		thenResultisNotEmpty();
		thenResultNameIs("david");
	}

	private void thenResultNameIs(String excepectedName) {
		assertSame(attendeeResult.get().getName(), excepectedName);
	}

	private void thenResultisNotEmpty() {
		assertThat(attendeeResult.isPresent(), is(true));
	}

	@Test
	public void testGetAttendeeByNameLastNameAndCompany_givenMatchingName_ThenReturnsCorrectAttendee() {
		givenAttendeeWithNameLastNameCompany("david", "gonzalez", "telefonica");
		whenGetAttendeeByNameLastName("david", "gonzalez");
		thenResultisNotEmpty();
		thenResultNameLastNameCompanyIs("david", "gonzalez", "telefonica");
	}

	private void givenAttendeeWithNameLastNameCompany(String name, String lastName, String company) {
		victim.saveAttendee(DomainBuilder.buildAttendee(name, lastName, company));
	}

	private void whenGetAttendeeByNameLastName(String name,  String lastName) {
		attendeeResult = victim.getAttendeeByNameAndLastName(name, lastName);
	}
	
	private void thenResultNameLastNameCompanyIs(String excepectedName, String expectedLastName, String expectedCompany) {
		assertSame(attendeeResult.get().getName(), excepectedName);
		assertSame(attendeeResult.get().getLastName(), expectedLastName);
		assertSame(attendeeResult.get().getCompany(), expectedCompany);
	}

}